# Merge Request para barrio check

## NOMBRE DE MI CONTRIBUCIÓN

Breve descripción de la contribución (de 1 a 4 líneas).

Issue al que corresponde (en caso de que resuelva o contribuya a algún issue): 

Lista de comprobación:

- [ ] He comprobado que funciona en mi máquina
- [ ] Es parte de un issue ya creado
- [ ] Es un merge request completo

Notas extra, opiniones, etc




Ejemplo de contribución:

## Añado datos

Hola, he recopilado datos que pueden encajar para mi barrio. 

Issue al que corresponde: 3

Lista de comprobación:

- [ ] He comprobado que funciona en mi máquina
- [X] Es parte de un issue ya creado
- [X] Es un merge request completo

No se como usar los datos para que el código del ejemplo funcione, pero aún así espero que sea de ayuda.

Un saludo!








