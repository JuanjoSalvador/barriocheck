# Barrio Check

## Finalidad

Éste es un proyecto en curso, la idea es crear una interfaz rápida y sencilla para detectar zonas inseguras en diferentes barrios, y definir lugares LGBT-friendly, no racistas, no machistas, etc.

La herramienta está pensada para lanzar un portal con información básica sobre cómo mandar avisos y otro botón que lleve al mapa modificado.

![](https://media.giphy.com/media/SvLzvdiM6w5EUrm7Es/giphy.gif)

## ¿Cómo funciona?

En este repo solo está el boceto con ejemplos de prueba, no hay datos reales implementados. Para probarlo, desde la carpeta principal ejecutar:

```
python3 crear_mapa.py
```

Y abrir el index.html en el navegador. Consultando el mapa, al cual se accede con el botón que lo indica en el portal, pueden verse los ejemplos.

Los datos de ejemplo están en carpeta "ejemplo" donde hay un txt con los datos manejados, en este caso aleatorios de un barrio de Madrid. Para añadir más ejemplos y coordenadas, sólo hace falta ampliar/modificar ese documento. Cada vez que se añaden datos nuevos en el documento de ejemplo, hay que actualizar el mapa ejecutando `crear_mapa.py`. 

## Pendientes

* Probar con datos reales en un barrio.
* Incluir instrucciones de despliegue en gitlab.io o Heroku+ruby on rails.
* Manual extendido.
* Crear un portal de administración friendly para añadir datos con una interfaz gráfica.

## Hacktoberfest 2021

¿Quieres conseguir un PR para [Hacktoberfest](https://hacktoberfest.digitalocean.com)? He creado varios issues con diferentes tipos de participación de varios niveles, y además ayudaría a mover este proyecto pensado para barrios.

Cualquier merge request será revisado, pero tienes [aquí](https://gitlab.com/terceranexus6/barriocheck/-/blob/master/merge_request_template.md) un template para crear tu merge request que sea más cómodo para mi de revisar y a ti de hacer sin saltarte ninguna información.

FAQ:

**¿Qué es el Hacktoberfest?**
Es un reto de softwarelibre que transcurre en octubre y está pensado para mejorar proyectos a cambio de conseguir una camiseta. Tienes más info [aquí](https://hacktoberfest.digitalocean.com).

**¿Qué es un merge request?**
En un proyecto normalmente ajeno, un merge request (o un pull request, en github) se trata de una propuesta de mejora, una sujerencia que se manda a la/el maintainer (persona responsable) del proyecto. Puede ser desde una idea nueva, una falta de ortografía corregida, un botón de colores o un sistema de seguridad. Lo mejor es fijarse en los "issues" y descubrir que cosas hay pendientes por mejorar.

**¿Qué es un issue?**
Un [issue](https://gitlab.com/terceranexus6/barriocheck/-/issues) son cosas pendientes por hacer en un proyecto libre. La persona con rol de mantainer a veces es consciente de las mejoras pendientes pero no tiene tiempo/conocimientos/recursos para llevarlas a cabo. Ahí es donde entra la comunidad, aportanto ayuda con lo que se pueda. 


Si tienes alguna duda puedes escribirme a inversealien@protonmail.com. 

![](https://hacktoberfest.digitalocean.com/_nuxt/img/logo-hacktoberfest-full.f42e3b1.svg)
